#include "systemfontprovider.h"

SystemFontProvider::SystemFontProvider(const QString & filePath, const PaletteProvider * hudPaletteProvider, DataVersion version, DataProvider * parent)
    : FontProvider(filePath, hudPaletteProvider, version, parent)
{
}

int SystemFontProvider::transformIndex(int idx) const
{
    const int paletteOffset = 0xC4;
    return idx - 0x10 + paletteOffset;
}
