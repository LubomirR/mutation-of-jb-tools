#include "directoryprovider.h"
#include "sceneprovider.h"
#include "roomfileprovider.h"
#include "animationpackageprovider.h"
#include "systemfontprovider.h"
#include "speechfontprovider.h"

#include <QDir>
#include <QString>

DirectoryProvider::DirectoryProvider(const QString & dir, DataVersion version, QObject * parent)
    : DataProvider(version, parent),
      m_directory(dir)
{
}

DataProviderType DirectoryProvider::type()
{
    return DataProviderType::Directory;
}

QString DirectoryProvider::name()
{
    return tr("JB directory");
}

DataProviders DirectoryProvider::buildChildren()
{
    DataProviders providers;
    QDir dir(m_directory);
    QStringList filters;
    filters << "ROOM*.DAT";

    const QStringList roomFiles = dir.entryList(filters, QDir::Files, QDir::Name);
    for (const QString & room : roomFiles) {
        QString sceneId = room;
        sceneId.chop(4); // Remove .DAT
        sceneId.remove(0, 4); // Remove ROOM

        providers.push_back(new SceneProvider(sceneId, m_version, this));
    }

    const QStringList specialRoomFiles = {"PRICHOD.DAT", "NEOLOGO.DAT", "RAKETA.DAT", "PC1.DAT", "PUZZLE.DAT", "LOGO2.DAT", "ICONS.DAT", "WELCM.DAT", "STRT.DAT"};

    for (const QString & room : specialRoomFiles) {
        if (dir.exists(room)) {
            providers.push_back(new RoomFileProvider(dir.filePath(room), m_version, this));
        }
    }

    filters = QStringList();
    filters << "*.APK";

    const QStringList apkFiles = dir.entryList(filters, QDir::Files, QDir::Name);
    for (const QString & apkFile : apkFiles) {
        providers.push_back(new AnimationPackageProvider(dir.filePath(apkFile), m_version, this));
    }

    filters = QStringList();
    filters << "*.AFT";

    // Fonts
    {
        RoomFileProvider * room0 = new RoomFileProvider(dir.filePath("ROOM0.DAT"), m_version, this);
        room0->buildChildren();
        PaletteProvider * hudPaletteProvider = room0->paletteProvider();
        Q_ASSERT(hudPaletteProvider);

        providers.push_back(new SpeechFontProvider(dir.filePath("FONT1.AFT"), hudPaletteProvider, m_version, this));
        providers.push_back(new SystemFontProvider(dir.filePath("SYSFNT.AFT"), hudPaletteProvider, m_version, this));
    }

    return providers;
}

const QString & DirectoryProvider::directory() const
{
    return m_directory;
}

