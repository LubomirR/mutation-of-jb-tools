#ifndef CP895CODEC_H
#define CP895CODEC_H

#include <QTextCodec>

class CP895Codec : public QTextCodec
{
    public:
        CP895Codec();

        virtual QByteArray name() const override;
        virtual int mibEnum() const override;

    protected:
        virtual QString convertToUnicode(const char * chars, int len, ConverterState * state) const override;
        virtual QByteArray convertFromUnicode(const QChar *input, int number, ConverterState *state) const override;

};

#endif // CP895CODEC_H
