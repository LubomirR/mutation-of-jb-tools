#include "roomanimationprovider.h"
#include "roomfileprovider.h"
#include "roombackgroundprovider.h"
#include "encryptedfile.h"

#include <QImage>
#include <QDebug>
#include <memory>

RoomAnimationProvider::RoomAnimationProvider(const QString & filePath, qint64 seekPos, qint64 length, DataVersion version, RoomFileProvider * parent)
    : DataProvider(version, parent),
      m_filePath(filePath),
      m_seekPos(seekPos),
      m_length(length)
{
}

DataProviderType RoomAnimationProvider::type()
{
    return DataProviderType::AnimationFrame;
}

QString RoomAnimationProvider::name()
{
    return tr("Animation Frame");
}

QImage RoomAnimationProvider::readImage() const
{
    RoomFileProvider * const roomProvider = static_cast<RoomFileProvider *>(parent());
    if (!roomProvider)
        return QImage();

    RoomBackgroundProvider * const backgroundProvider = roomProvider->backgroundProvider();
    if (!backgroundProvider)
        return QImage();

    QImage image = backgroundProvider->readImage();
    if (image.isNull())
        return QImage();

    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly))
        return QImage();

    encFile.seek(m_seekPos);

    quint16 firstLine;
    quint16 numLines;
    QDataStream dataStream(&encFile);
    dataStream.setByteOrder(QDataStream::LittleEndian);
    dataStream >> firstLine >> numLines;

    for (quint16 line = firstLine; line < firstLine + numLines; ++line)
    {
        quint8 * imageData = image.scanLine(line);

        quint8 runs = 0;
        dataStream >> runs;
        while(runs--)
        {
            quint8 localOffset = 0;
            dataStream >> localOffset;
            imageData += localOffset;

            quint8 num = 0;
            dataStream >> num;

            if (num < 0x80) {
                std::unique_ptr<quint8[]> data(new quint8[num]);
                const int read = dataStream.readRawData(reinterpret_cast<char *>(data.get()), num);
                if (read != num) {
                    qWarning() << "Not enough data.";
                    return QImage();
                }
                memcpy(imageData, data.get(), num);
                imageData += num;
            } else {
                int no = 0x100 - num;
                quint8 color = 0;
                dataStream >> color;
                while(no--) {
                    *imageData = color;
                    imageData++;
                }
            }
        }
    }

    return image;
}
