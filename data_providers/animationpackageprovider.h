#ifndef ANIMATIONPACKAGEPROVIDER_H
#define ANIMATIONPACKAGEPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

class AnimationPackageProvider : public DataProvider
{
    Q_OBJECT

    public:
        AnimationPackageProvider(const QString & filePath, DataVersion version, DataProvider * parent = nullptr);
        virtual DataProviderType type() override;
        virtual QString name() override;

        QList<QImage> readImages() const;

    private:
        QString m_filePath;

};

#endif // ANIMATIONPACKAGEPROVIDER_H
