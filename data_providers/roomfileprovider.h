#ifndef ROOMFILEPROVIDER_H
#define ROOMFILEPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

class PaletteProvider;
class RoomBackgroundProvider;

class RoomFileProvider : public DataProvider
{
    Q_OBJECT

    public:
        RoomFileProvider(const QString & filePath, DataVersion version, DataProvider * parent = nullptr);
        virtual DataProviderType type() override;
        virtual QString name() override;

        virtual DataProviders buildChildren() override;

        PaletteProvider * paletteProvider() const;
        RoomBackgroundProvider * backgroundProvider() const;

    private:
        QString m_filePath;
        PaletteProvider * m_paletteProvider = nullptr;
        RoomBackgroundProvider * m_backgroundProvider = nullptr;
};

#endif // ROOMFILEPROVIDER_H
