#include "sceneprovider.h"
#include "directoryprovider.h"
#include "scriptfileprovider.h"
#include "roomfileprovider.h"

#include <QDir>

SceneProvider::SceneProvider(const QString & sceneId, DataVersion version, DirectoryProvider * parent)
    : DataProvider(version, parent),
      m_sceneId(sceneId)
{

}

DataProviderType SceneProvider::type()
{
    return DataProviderType::Scene;
}

QString SceneProvider::name()
{
    return tr("Scene %1").arg(m_sceneId);
}

DataProviders SceneProvider::buildChildren()
{
    DataProviders providers;

    QDir dir(static_cast<DirectoryProvider *>(parent())->directory());
    const QString roomFileName = QString("ROOM%1.DAT").arg(m_sceneId);
    const QString scriptFileName = QString("SCRN%1.ATN").arg(m_sceneId);

    if (dir.exists(roomFileName))
    {
        providers.push_back(new RoomFileProvider(dir.filePath(roomFileName), m_version, this));
    }

    if (dir.exists(scriptFileName))
    {
        providers.push_back(new ScriptFileProvider(dir.filePath(scriptFileName), m_version, this));
    }

    return providers;
}
