#ifndef DIRECTORYPROVIDER_H
#define DIRECTORYPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

class DirectoryProvider : public DataProvider
{
    Q_OBJECT

    public:
        DirectoryProvider(const QString & dir, DataVersion version, QObject * parent = nullptr);
        virtual DataProviderType type() override;
        virtual QString name() override;

        virtual DataProviders buildChildren() override;

        const QString & directory() const;

    private:
        QString m_directory;
};

#endif // DIRECTORYPROVIDER_H
