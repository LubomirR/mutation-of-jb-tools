#include "roombackgroundprovider.h"
#include "roomfileprovider.h"
#include "paletteprovider.h"
#include "encryptedfile.h"

#include <QImage>
#include <QDebug>

RoomBackgroundProvider::RoomBackgroundProvider(const QString & filePath, qint64 seekPos, qint64 length, DataVersion version, RoomFileProvider * parent)
    : DataProvider(version, parent),
      m_filePath(filePath),
      m_seekPos(seekPos),
      m_length(length)
{
}

DataProviderType RoomBackgroundProvider::type()
{
    return DataProviderType::BackgroundImage;
}

QString RoomBackgroundProvider::name()
{
    return tr("Background");
}

QImage RoomBackgroundProvider::readImage() const
{
    RoomFileProvider * const roomProvider = static_cast<RoomFileProvider *>(parent());
    if (!roomProvider)
        return QImage();

    PaletteProvider * const paletteProvider = roomProvider->paletteProvider();
    if (!paletteProvider)
        return QImage();

    const QVector<QRgb> palette = paletteProvider->readPalette();

    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly))
        return QImage();

    encFile.seek(m_seekPos);

    qint64 length = m_length;

    const int width = 320;
    const int height = 200;

    QImage image(width, height, QImage::Format_Indexed8);
    image.fill(0);
    image.setColorTable(palette);

    quint8 * imageData = image.bits();
    int imagePos = 0;
    const int imageDataLen = width * height;

    QFile decoded("TestFile.bin");
    decoded.open(QIODevice::WriteOnly);

    int numNum = 0;

    for (int encDataPos = 0; encDataPos < length; ) {
        if (imagePos % 320 == 0)
        {
            quint8 dummy = 0;
            const qint64 read = encFile.read(reinterpret_cast<char *>(&dummy), 1);
            if (read != 1) {
                qWarning() << "Missing image data.";
                return image;
            }
            numNum = 0;
            encDataPos++;
            if (encDataPos == length)
                return image;
        }

        quint8 number = 0;
        const qint64 read = encFile.read(reinterpret_cast<char *>(&number), 1);
        if (read != 1) {
            qWarning() << "Missing image data.";
            return image;
        }
        encDataPos++;
        numNum++;

        if (number < 0x80) {
            quint8 color = 0;
            const qint64 read = encFile.read(reinterpret_cast<char *>(&color), 1);
            if (read != 1) {
                qWarning() << "Missing image data.";
                return image;
            }
            encDataPos++;
            if (number != 0)
            {
                for (int i = 0; i < number; ++i) {
                    decoded.write(reinterpret_cast<const char *>(&color), 1);
                    if (imagePos >= imageDataLen) {
                        qWarning() << "Image data too big.";
                        return image;
                    }
                    imageData[imagePos++] = color;
                }
            } else {
                qWarning() << "Unexpected 0-length RLE chunk.";
            }
        } else {
            const int no = 0x100 - number;
            QByteArray data = encFile.read(no);
            for (char & ch : data) {
                decoded.write(&ch, 1);
                if (imagePos >= imageDataLen) {
                    qWarning() << "Image data too big.";
                    return image;
                }
                imageData[imagePos++] = reinterpret_cast<quint8 &>(ch);
            }
            if (data.size() != no) {
                qWarning() << "Missing image data.";
                return image;
            }
            encDataPos += no;
        }
    }

    return image;
}

