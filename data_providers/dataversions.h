#ifndef DATAVERSIONS_H
#define DATAVERSIONS_H

enum class DataVersion : int {
    Demo = 0,
    Full,
    NoVersions
};

#endif // DATAVERSIONS_H
