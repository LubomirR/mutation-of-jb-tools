#include "fontprovider.h"
#include "encryptedfile.h"

#include <QDebug>
#include <QDataStream>
#include <QRgb>
#include <QFileInfo>
#include <QTextCodec>

FontProvider::FontProvider(const QString & filePath, const PaletteProvider * hudPaletteProvider, DataVersion version, DataProvider * parent)
    : DataProvider(version, parent),
      m_filePath(filePath),
      m_hudPaletteProvider(hudPaletteProvider)
{
}

DataProviderType FontProvider::type()
{
    return DataProviderType::Font;
}

QString FontProvider::name()
{
    QFileInfo fileInfo(m_filePath);
    QString baseName = fileInfo.baseName();

    return tr("Font %1").arg(baseName);
}

QVector<QPair<QChar, QImage>> FontProvider::readGlyphs() const
{
    QVector<QPair<QChar, QImage>> glyphList;

    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Failed to open AFT file.";
        return glyphList;
    }

    QVector<QRgb> palette = m_hudPaletteProvider->readPalette();
    palette[0] = qRgb(0xFF, 0x00, 0xFF); // Transparent color! In data file, it's encoded as 0, but it is not treated as index to palette.

    encFile.seek(0x02D6); // skip header + unknown (likely unused) data

    QDataStream dataStream(&encFile);
    dataStream.setByteOrder(QDataStream::LittleEndian);

    quint16 noGlyphs = 0; // number of glyphs
    dataStream >> noGlyphs;

    dataStream.skipRawData(7); // unknown data (0s for all existing fonts)

    for (int i = 0; i < noGlyphs; i++) {
        quint8 character = 0; // CP895 character
        dataStream >> character;

        quint8 glyphWidth = 0, glyphHeight = 0;
        dataStream >> glyphWidth >> glyphHeight;

        QImage glyph(glyphWidth, glyphHeight, QImage::Format_Indexed8);
        glyph.fill(0);
        glyph.setColorTable(palette);

        for (int r = 0; r < glyphHeight; r++) {
            uchar * const rowData = glyph.scanLine(r);
            dataStream.readRawData(reinterpret_cast<char*>(rowData), glyphWidth);

            for (int c = 0; c < glyphWidth; c++) {
                if (rowData[c] != 0)
                    rowData[c] = transformIndex(rowData[c]);
            }
        }

        if (dataStream.status() != QDataStream::Ok) {
            qWarning() << "Failed to load glyph.";
            break;
        }

        QString converted = QTextCodec::codecForName("CP859")->toUnicode(reinterpret_cast<const char*>(&character), 1);
        glyphList.push_back(qMakePair(converted.at(0), glyph));
    }

    return glyphList;
}





