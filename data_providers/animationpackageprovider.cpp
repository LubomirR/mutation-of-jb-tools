#include "animationpackageprovider.h"
#include "encryptedfile.h"

#include <QDebug>
#include <QDataStream>
#include <QRgb>
#include <QImage>
#include <QFileInfo>

AnimationPackageProvider::AnimationPackageProvider(const QString & filePath, DataVersion version, DataProvider * parent)
    : DataProvider(version, parent),
      m_filePath(filePath)
{

}

DataProviderType AnimationPackageProvider::type()
{
    return DataProviderType::AnimationPackage;
}

QString AnimationPackageProvider::name()
{
    QFileInfo fileInfo(m_filePath);
    QString baseName = fileInfo.baseName();

    return tr("Character Animation %1").arg(baseName);
}

QList<QImage> AnimationPackageProvider::readImages() const
{
    QList<QImage> imageList;

    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Failed to open APK file.";
        return imageList;
    }

    constexpr int COLOR_ENTRIES = 16;
    constexpr int IMAGE_WIDTH = 0x1E * 2;
    constexpr int IMAGE_HEIGHT = 0x3B;
    constexpr int IMAGE_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT;

    encFile.seek(0x06); // Skip header
    QByteArray paletteData = encFile.read(COLOR_ENTRIES * 3); // Read palette
    const quint8 * rawPaletteData = reinterpret_cast<const quint8*>(paletteData.constData());

    QVector<QRgb> palette;
    palette.reserve(COLOR_ENTRIES + 1);
    palette.push_back(qRgb(0xD0, 0xD0, 0xD0)); // Transparent color! In data file, it's encoded as 0, but it is not treated as index to palette.

    for (int i = 0; i < COLOR_ENTRIES; ++i)
    {
        const int r = rawPaletteData[3 * i] << 2;
        const int g = rawPaletteData[3 * i + 1] << 2;
        const int b = rawPaletteData[3 * i + 2] << 2;

        palette.push_back(qRgb(r, g, b));
    }

    encFile.seek(0x02D6); // Frame lengths

    QDataStream dataStream(&encFile);
    dataStream.setByteOrder(QDataStream::LittleEndian);

    QVector<quint16> frameLengths;
    quint16 numberOfLength = 0;
    dataStream >> numberOfLength;

    frameLengths.reserve(numberOfLength);
    while(numberOfLength--) {
        quint16 length = 0;
        dataStream >> length;
        frameLengths.push_back(length);
    }

    for (const int length : frameLengths) {
        int readBytes = 0;
        QImage image(IMAGE_WIDTH, IMAGE_HEIGHT, QImage::Format_Indexed8);
        image.fill(0);
        image.setColorTable(palette);
        uchar * const imageData = image.bits();
        int imageDataIndex = 0;

        while (readBytes != length)
        {
            quint8 data[2] = {0};
            encFile.read(reinterpret_cast<char *>(data), 2);
            readBytes += 2;

            while(data[0]--) {
                if (imageDataIndex >= IMAGE_SIZE) {
                    qWarning() << "Too much data.";
                    break;
                }
                if (data[1] == 0)
                    imageData[imageDataIndex] = 0;
                else
                    imageData[imageDataIndex] = data[1] - 0x0F;

                imageDataIndex++;
            }
        }

        if (imageDataIndex != IMAGE_SIZE)
        {
            qWarning() << "Animation image does not have correct size.";
            continue;
        }

        imageList.push_back(image);
    }

    return imageList;
}
