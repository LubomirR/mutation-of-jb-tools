#include "scriptfileprovider.h"
#include "encryptedfile.h"
#include "cp895codec.h"

#include <QTextStream>
#include <QDebug>

constexpr int UNKNOWN_DATA_LENGTH = 126;

ScriptFileProvider::ScriptFileProvider(const QString & filePath, DataVersion version, DataProvider * parent)
    : DataProvider(version, parent),
      m_filePath(filePath)
{
}

DataProviderType ScriptFileProvider::type()
{
    return DataProviderType::ScriptFile;
}

QString ScriptFileProvider::name()
{
    return tr("Script");
}

QString ScriptFileProvider::readScript() const
{
    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly))
        return QString();

    quint8 ch = 0;
    enum class State
    {
        Start,
        FoundCR,
        End
    };

    State state = State::Start;

    while (state != State::End) {
        if (!encFile.getChar(reinterpret_cast<char *>(&ch))) {
            qWarning() << "Not enough data in script file.";
            return QString();
        }
        if (state == State::Start) {
            if (ch == 0x0D)
                state = State::FoundCR;
        } else if (state == State::FoundCR) {
            if (ch == 0x0A)
                state = State::End;
            else
                state = State::Start;
        }
    }

    // If CRLF is present right at the beginning, show the whole file (see SCRN0.ATN for an example).
    // Otherwise we have to skip unknown data (seems to be always 126 bytes).
    if (encFile.pos() != 2)
        encFile.seek(encFile.pos() + UNKNOWN_DATA_LENGTH);

    QTextStream textStream(&encFile);
    textStream.setCodec("CP859");
    QString script = textStream.readAll();

    return script.replace("\r\n", "\n");
}

bool ScriptFileProvider::writeScript(const QString & script)
{
    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly))
        return false;

    QByteArray oldDataBuf;

    quint8 ch = 0;
    enum class State
    {
        Start,
        FoundCR,
        End
    };

    State state = State::Start;

    while (state != State::End) {
        if (!encFile.getChar(reinterpret_cast<char *>(&ch))) {
            qWarning() << "Not enough data in script file.";
            return false;
        }
        if (state == State::Start) {
            if (ch == 0x0D)
                state = State::FoundCR;
        } else if (state == State::FoundCR) {
            if (ch == 0x0A)
                state = State::End;
            else
                state = State::Start;
        }
        oldDataBuf.append(reinterpret_cast<const char*>(&ch), 1);
    }

    const int oldSize = oldDataBuf.size();
    oldDataBuf.resize(oldSize + UNKNOWN_DATA_LENGTH);
    if (encFile.read(oldDataBuf.data() + oldSize, UNKNOWN_DATA_LENGTH) != UNKNOWN_DATA_LENGTH)
        return false;
    encFile.close();

    QString scriptWithCRLF = script;
    scriptWithCRLF.replace("\n", "\r\n");

    if (!encFile.open(QIODevice::WriteOnly))
        return false;

    encFile.write(oldDataBuf.constData(), oldDataBuf.size());

    QTextStream textStream(&encFile);
    textStream.setCodec("CP859");
    textStream << scriptWithCRLF;
    encFile.close();

    return true;
}
