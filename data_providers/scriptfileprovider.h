#ifndef SCRIPTFILEPROVIDER_H
#define SCRIPTFILEPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

class ScriptFileProvider : public DataProvider
{
    Q_OBJECT

    public:
        ScriptFileProvider(const QString & filePath, DataVersion version, DataProvider * parent = nullptr);
        virtual DataProviderType type() override;
        virtual QString name() override;

        QString readScript() const;
        bool writeScript(const QString & script);

    private:
        QString m_filePath;
};

#endif // SCRIPTFILEPROVIDER_H
