#ifndef ENCRYPTEDFILE_H
#define ENCRYPTEDFILE_H

#include <QFile>
#include "dataversions.h"

class EncryptedFile : public QFile
{
    Q_OBJECT

    public:
        EncryptedFile(const QString & name, DataVersion version);
        ~EncryptedFile() {}

    protected:
        virtual qint64 readData(char * data, qint64 len) override;
        virtual qint64 writeData(const char * data, qint64 len) override;

    private:
        const unsigned char * xorTable();
        DataVersion m_version;
};

#endif // ENCRYPTEDFILE_H
