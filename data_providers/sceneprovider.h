#ifndef SCENEPROVIDER_H
#define SCENEPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

class DirectoryProvider;

class SceneProvider : public DataProvider
{
    Q_OBJECT

    public:
        SceneProvider(const QString & sceneId, DataVersion version, DirectoryProvider * parent);
        virtual DataProviderType type() override;
        virtual QString name() override;

        virtual DataProviders buildChildren() override;

    private:
        QString m_sceneId;
};

#endif // SCENEPROVIDER_H
