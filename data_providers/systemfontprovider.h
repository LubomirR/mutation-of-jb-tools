#ifndef SYSTEMFONTPROVIDER_H
#define SYSTEMFONTPROVIDER_H

#include "fontprovider.h"
#include "dataversions.h"

class SystemFontProvider : public FontProvider
{
    public:
        SystemFontProvider(const QString & filePath, const PaletteProvider * hudPaletteProvider, DataVersion version, DataProvider * parent = nullptr);

    protected:
        virtual int transformIndex(int idx) const override;
};

#endif // SYSTEMFONTPROVIDER_H
