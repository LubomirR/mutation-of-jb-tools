#include "paletteprovider.h"
#include "encryptedfile.h"

#include <QDebug>

PaletteProvider::PaletteProvider(const QString & filePath, qint64 seekPos, qint64 length, DataVersion version, DataProvider * parent)
    : DataProvider(version, parent),
      m_filePath(filePath),
      m_seekPos(seekPos),
      m_length(length)
{
}

DataProviderType PaletteProvider::type()
{
    return DataProviderType::Palette;
}

QString PaletteProvider::name()
{
    return tr("Palette");
}

QVector<QRgb> PaletteProvider::readPalette() const
{
    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly))
        return QVector<QRgb>();

    // Skip first 4 bytes.
    encFile.seek(m_seekPos + 4);
    QByteArray data = encFile.read(m_length - 4);

    QVector<QRgb> palette;
    palette.reserve(data.size() / 3);
    const quint8 * rawData = reinterpret_cast<const quint8*>(data.constData());

    if (data.size() % 3 != 0)
    {
        qWarning() << "Incorrect palette size.";
    }

    for (int i = 0; i < data.size() / 3; ++i)
    {
        const int r = rawData[3 * i] << 2;
        const int g = rawData[3 * i + 1] << 2;
        const int b = rawData[3 * i + 2] << 2;

        palette.push_back(qRgb(r, g, b));
    }

    return palette;
}
