#ifndef FONTPROVIDER_H
#define FONTPROVIDER_H

#include "dataprovider.h"
#include "paletteprovider.h"
#include "dataversions.h"

#include <QVector>
#include <QPair>
#include <QChar>
#include <QImage>

class FontProvider : public DataProvider
{
    Q_OBJECT

    public:
        FontProvider(const QString & filePath, const PaletteProvider * hudPaletteProvider, DataVersion version, DataProvider * parent = nullptr);
        virtual DataProviderType type() override;
        virtual QString name() override;

        QVector<QPair<QChar, QImage>> readGlyphs() const;

    protected:
        virtual int transformIndex(int idx) const = 0;

    private:
        QString m_filePath;
        const PaletteProvider * m_hudPaletteProvider;
};

#endif // FONTPROVIDER_H
