#ifndef DUMMYPROVIDER_H
#define DUMMYPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

class DummyProvider : public DataProvider
{
    Q_OBJECT

    public:
        DummyProvider(const QString & title, DataVersion version, DataProvider * parent = nullptr);

        virtual DataProviderType type() override;
        virtual QString name() override;

    private:
        QString mTitle;
};

#endif // DUMMYPROVIDER_H
