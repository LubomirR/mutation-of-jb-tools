#ifndef SPEECHFONTPROVIDER_H
#define SPEECHFONTPROVIDER_H

#include "fontprovider.h"
#include "dataversions.h"

class SpeechFontProvider : public FontProvider
{
    public:
        SpeechFontProvider(const QString & filePath, const PaletteProvider * hudPaletteProvider, DataVersion version, DataProvider * parent = nullptr);

    protected:
        virtual int transformIndex(int idx) const override;
};

#endif // SPEECHFONTPROVIDER_H
