#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H

#include <QObject>
#include <QVector>

#include "dataversions.h"

enum class DataProviderType
{
    Unknown,
    Directory,
    Scene,
    RoomFile,
    ScriptFile,
    VoiceFile,
    BackgroundImage,
    Palette,
    AnimationFrame,
    AnimationPackage, // .apk files
    Font // .aft files
};

class DataProvider;
typedef QVector<DataProvider *> DataProviders;

class DataProvider : public QObject
{
    Q_OBJECT

    public:
        explicit DataProvider(DataVersion version, QObject * parent = nullptr);
        virtual DataProviderType type() = 0;
        virtual QString name() = 0;

        virtual DataProviders buildChildren();

    protected:
        DataVersion m_version;

    signals:

    public slots:
};

#endif // DATAPROVIDER_H
