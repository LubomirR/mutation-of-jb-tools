#include "speechfontprovider.h"

SpeechFontProvider::SpeechFontProvider(const QString & filePath, const PaletteProvider * hudPaletteProvider, DataVersion version, DataProvider * parent)
    : FontProvider (filePath, hudPaletteProvider, version, parent)
{
}

int SpeechFontProvider::transformIndex(int idx) const
{
    if (idx == 0x11) {
        return 0xC0; // the original game does this hack to keep font borders black
    }

    enum {
        WHITE = 0xC6,
        DARKGRAY = 0xC2,
        LIGHTGRAY = 0xC4,
        GREEN = 0xC8,
        ORANGE = 0xCA,
        DARKBLUE = 0xD6,
        LIGHTBLUE = 0xDA,
        BROWN = 0xDC
    };

    const int paletteOffset = WHITE;
    return idx  - 0x10 + paletteOffset;
}
