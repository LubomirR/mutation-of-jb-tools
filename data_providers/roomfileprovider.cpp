#include "roomfileprovider.h"
#include "encryptedfile.h"
#include "paletteprovider.h"
#include "roombackgroundprovider.h"
#include "roomanimationprovider.h"
#include "dummyprovider.h"

#include <QDataStream>
#include <QFileInfo>
#include <QDebug>

RoomFileProvider::RoomFileProvider(const QString & filePath, DataVersion version, DataProvider * parent)
    : DataProvider(version, parent),
      m_filePath(filePath)
{

}

DataProviderType RoomFileProvider::type()
{
    return DataProviderType::RoomFile;
}

QString RoomFileProvider::name()
{
    QFileInfo fileInfo(m_filePath);
    return tr("Room %1").arg(fileInfo.fileName());
}

DataProviders RoomFileProvider::buildChildren()
{
    DataProviders providers;

    EncryptedFile encFile(m_filePath, m_version);
    if (!encFile.open(QIODevice::ReadOnly))
        return providers;

    QDataStream dataStream(&encFile);
    dataStream.setByteOrder(QDataStream::LittleEndian);

    dataStream.skipRawData(0x80);

    while (!encFile.atEnd()) {
        quint32 length = 0;
        dataStream >> length;

        quint8 info[4] = {};
        dataStream.readRawData(reinterpret_cast<char *>(info), 4);

        quint32 dummy;
        dataStream >> dummy;
        dataStream >> dummy;

        if (info[0] == 0xFA && info[1] == 0xF1) {
            // Subrecords.
            if (info[2] == 0)
            {
                qInfo() << "Empty record.";
                providers.push_back(new DummyProvider(tr("<Empty record>"), m_version, this));
            } else if (info[2] > 2) {
                qInfo() << "Big record.";
            }
            for (int i = 0; i < info[2]; ++i) {
                quint32 sublength = 0;
                dataStream >> sublength;
                quint16 type = 0;
                dataStream >> type;

                if (type == 0x0B) {
                    if (m_paletteProvider)
                        qInfo() << "Palette already exists.";

                    m_paletteProvider = new PaletteProvider(m_filePath, encFile.pos(), sublength - 6, m_version, this);
                    providers.push_back(m_paletteProvider);
                } else if (type == 0x0C) {
                    providers.push_back(new RoomAnimationProvider(m_filePath, encFile.pos(), sublength - 6, m_version, this));
                } else if (type == 0x0F) {
                    if (m_backgroundProvider)
                        qInfo() << "Background already exists.";

                    m_backgroundProvider = new RoomBackgroundProvider(m_filePath, encFile.pos(), sublength - 6, m_version, this);
                    providers.push_back(m_backgroundProvider);
                } else {
                    qInfo() << "Unknown subrecord type.";
                }

                dataStream.skipRawData(sublength - 6);
            }
        } else {
            qInfo() << "Unknown record type.";
            dataStream.skipRawData(length - 16);
        }
    }

    return providers;
}

PaletteProvider *RoomFileProvider::paletteProvider() const
{
    return m_paletteProvider;
}

RoomBackgroundProvider *RoomFileProvider::backgroundProvider() const
{
    return m_backgroundProvider;
}
