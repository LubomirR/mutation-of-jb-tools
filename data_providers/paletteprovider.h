#ifndef PALETTEPROVIDER_H
#define PALETTEPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

#include <QColor>

class PaletteProvider : public DataProvider
{
    Q_OBJECT

    public:
        PaletteProvider(const QString & filePath, qint64 seekPos, qint64 length, DataVersion version, DataProvider * parent = nullptr);
        virtual DataProviderType type() override;
        virtual QString name() override;

        QVector<QRgb> readPalette() const;

    private:
        QString m_filePath;
        qint64 m_seekPos;
        qint64 m_length;
};

#endif // PALETTEPROVIDER_H
