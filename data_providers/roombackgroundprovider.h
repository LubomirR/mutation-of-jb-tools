#ifndef IMAGEPROVIDER_H
#define IMAGEPROVIDER_H

#include "dataprovider.h"
#include "dataversions.h"

class RoomFileProvider;

class RoomBackgroundProvider : public DataProvider
{
    Q_OBJECT

    public:
        RoomBackgroundProvider(const QString & filePath, qint64 seekPos, qint64 length, DataVersion version, RoomFileProvider * parent = nullptr);
        virtual DataProviderType type() override;
        virtual QString name() override;

        QImage readImage() const;

    private:
        QString m_filePath;
        qint64 m_seekPos;
        qint64 m_length;
};

#endif // IMAGEPROVIDER_H
