#include "dummyprovider.h"

DummyProvider::DummyProvider(const QString & title, DataVersion version, DataProvider * parent)
    : DataProvider(version, parent),
      mTitle(title)
{
}

DataProviderType DummyProvider::type()
{
    return DataProviderType::Unknown;
}

QString DummyProvider::name()
{
    return mTitle;
}
