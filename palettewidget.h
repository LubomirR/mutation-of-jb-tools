#ifndef PALETTEWIDGET_H
#define PALETTEWIDGET_H

#include <QWidget>
#include <QColor>

class PaletteWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit PaletteWidget(QWidget * parent = nullptr);
        void setData(const QVector<QRgb> & palette);

        virtual QSize minimumSizeHint() const override;
        virtual QSize sizeHint() const override;

    protected:
        virtual void paintEvent(QPaintEvent * event) override;

    signals:

    public slots:

    private:
        QVector<QRgb> m_palette;
};

#endif // PALETTEWIDGET_H
