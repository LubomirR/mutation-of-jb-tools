#include "palettewidget.h"

#include <QPainter>

static constexpr int CELL_SIZE = 32;
static constexpr int COLORS = 256;

static constexpr int COLOR_ROWS = 16;
static constexpr int COLOR_COLS = 16;

PaletteWidget::PaletteWidget(QWidget * parent) : QWidget(parent)
{

}

void PaletteWidget::setData(const QVector<QRgb> & palette)
{
    m_palette = palette;
    update();
}

QSize PaletteWidget::minimumSizeHint() const
{
    return QSize(COLOR_COLS * CELL_SIZE, COLOR_ROWS * CELL_SIZE);
}

QSize PaletteWidget::sizeHint() const
{
    return minimumSizeHint();
}

void PaletteWidget::paintEvent(QPaintEvent * event)
{
    Q_UNUSED(event);

    if (m_palette.size() != COLORS)
        return;

    QPainter painter(this);
    for (int row = 0; row < COLOR_ROWS; ++row)
    {
        for (int col = 0; col < COLOR_COLS; ++col)
        {
            const QRgb rgb = m_palette[row * COLOR_COLS + col];

            painter.fillRect(col * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE, QColor::fromRgb(rgb));
        }
    }
}
