#-------------------------------------------------
#
# Project created by QtCreator 2017-12-26T12:27:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jbviewer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    data_providers/dataprovider.cpp \
    data_providers/directoryprovider.cpp \
    data_providers/sceneprovider.cpp \
    data_providers/roomfileprovider.cpp \
    data_providers/scriptfileprovider.cpp \
    data_providers/encryptedfile.cpp \
    data_providers/cp895codec.cpp \
    data_providers/paletteprovider.cpp \
    palettewidget.cpp \
    data_providers/roombackgroundprovider.cpp \
    data_providers/roomanimationprovider.cpp \
    data_providers/animationpackageprovider.cpp \
    data_providers/fontprovider.cpp \
    data_providers/systemfontprovider.cpp \
    data_providers/speechfontprovider.cpp \
    data_providers/dummyprovider.cpp

HEADERS += \
        mainwindow.h \
    data_providers/dataprovider.h \
    data_providers/directoryprovider.h \
    data_providers/sceneprovider.h \
    data_providers/roomfileprovider.h \
    data_providers/scriptfileprovider.h \
    data_providers/encryptedfile.h \
    data_providers/cp895codec.h \
    data_providers/paletteprovider.h \
    palettewidget.h \
    data_providers/roombackgroundprovider.h \
    data_providers/roomanimationprovider.h \
    data_providers/animationpackageprovider.h \
    data_providers/fontprovider.h \
    data_providers/systemfontprovider.h \
    data_providers/speechfontprovider.h \
    data_providers/dummyprovider.h \
    data_providers/dataversions.h

FORMS += \
        mainwindow.ui

CONFIG   += c++11

win32-g++ {
   QMAKE_CXXFLAGS += -Werror
}
win32-msvc* {
   QMAKE_CXXFLAGS += /WX
}
unix {
    QMAKE_CXXFLAGS += -Werror
}
