#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "data_providers/directoryprovider.h"
#include "data_providers/scriptfileprovider.h"
#include "data_providers/paletteprovider.h"
#include "data_providers/roombackgroundprovider.h"
#include "data_providers/roomanimationprovider.h"
#include "data_providers/animationpackageprovider.h"
#include "data_providers/fontprovider.h"
#include "data_providers/dataversions.h"

#include <QFileDialog>
#include <QFont>
#include <QFontDatabase>
#include <QMessageBox>
#include <QVector>
#include <QChar>
#include <QImage>
#include <QKeyEvent>
#include <QDir>

namespace {

DataVersion determineVersion(const QString & path)
{
    QDir dir(path);
    if (dir.exists("JBDEMO.EXE")) {
        return DataVersion::Demo;
    } else {
        return DataVersion::Full;
    }
}

}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    ui->scriptBrowser->setFont(fixedFont);
    ui->scriptBrowser->installEventFilter(this);
    ui->findEdit->installEventFilter(this);
    ui->findWidget->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_triggered()
{
    const QString dir = QFileDialog::getExistingDirectory(nullptr, tr("Locate JB directory"));
    if (dir.isEmpty())
        return;

    buildTree(dir);
}

static void buildChildren(QTreeWidgetItem * parentItem, DataProvider * parentProvider)
{
    const DataProviders & providers = parentProvider->buildChildren();

    for (DataProvider * const provider : providers) {
        QTreeWidgetItem * const treeItem = new QTreeWidgetItem;
        treeItem->setData(0, Qt::DisplayRole, provider->name());
        treeItem->setData(0, Qt::UserRole, QVariant::fromValue(provider));
        parentItem->addChild(treeItem);

        buildChildren(treeItem, provider);
    }
}

void MainWindow::buildTree(const QString & dir)
{
    ui->treeWidget->clear();
    DirectoryProvider * const dirProvider = new DirectoryProvider(dir, determineVersion(dir));
    buildChildren(ui->treeWidget->invisibleRootItem(), dirProvider);
}

void MainWindow::searchScriptBrowser()
{
    auto find = [this](const QString & subString) -> QTextCursor {
        QTextCursor search = ui->scriptBrowser->document()->find(subString, ui->scriptBrowser->textCursor());

        if (!search.isNull()) {
            ui->scriptBrowser->setTextCursor(search);
        } else {
            // wrap around

            QTextCursor search2 = ui->scriptBrowser->document()->find(subString);
            if (!search2.isNull()) {
                ui->scriptBrowser->setTextCursor(search2);
            }

            return search2;
        }

        return search;
    };

    if (!ui->findEdit->text().isEmpty()) {
        QPalette palette = ui->findEdit->palette();
        if (!find(ui->findEdit->text()).isNull()) {
            palette.setColor(QPalette::Base, QColor(212, 250, 226));
        } else {
            palette.setColor(QPalette::Base, QColor(249, 233, 234));
        }
        ui->findEdit->setPalette(palette);
    } else {
        ui->findEdit->setPalette(QApplication::palette());
    }
}

void MainWindow::on_treeWidget_currentItemChanged(QTreeWidgetItem * current, QTreeWidgetItem * previous)
{
    Q_UNUSED(previous);

    if (!current) {
        // current is nullptr when tree widget is cleared
        ui->stackedWidget->setCurrentIndex(0);
        return;
    }

    DataProvider* const provider = current->data(0, Qt::UserRole).value<DataProvider*>();
    if (!provider)
        return;

    switch (provider->type())
    {
        case DataProviderType::ScriptFile:
        {
            const QString script = static_cast<ScriptFileProvider *>(provider)->readScript();
            ui->scriptBrowser->setPlainText(script);
            ui->findWidget->hide();
            ui->findEdit->clear();
            ui->findEdit->setPalette(QApplication::palette());
            ui->stackedWidget->setCurrentIndex(1);
            break;
        }
        case DataProviderType::Palette:
        {
            ui->paletteWidget->setData(static_cast<PaletteProvider *>(provider)->readPalette());
            ui->stackedWidget->setCurrentIndex(2);
            break;
        }
        case DataProviderType::BackgroundImage:
        {
            const QImage image = static_cast<RoomBackgroundProvider *>(provider)->readImage();
            QPixmap pixmap;
            pixmap.convertFromImage(image);
            pixmap.setDevicePixelRatio(0.5);
            ui->image->setPixmap(pixmap);
            ui->stackedWidget->setCurrentIndex(3);
            break;
        }
        case DataProviderType::AnimationFrame:
        {
            const QImage image = static_cast<RoomAnimationProvider *>(provider)->readImage();
            QPixmap pixmap;
            pixmap.convertFromImage(image);
            pixmap.setDevicePixelRatio(0.5);
            ui->image->setPixmap(pixmap);
            ui->stackedWidget->setCurrentIndex(3);
            break;
        }
        case DataProviderType::AnimationPackage:
        {
            qDeleteAll(ui->scrollAreaWidgetContents->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));

            const QList<QImage> imageList = static_cast<AnimationPackageProvider *>(provider)->readImages();
            for (int i = 0; i < imageList.size(); ++i)
            {
                QLabel * const imageLabel = new QLabel;
                QLabel * const textLabel = new QLabel;
                ui->gridLayout->addWidget(imageLabel, i, 0);
                ui->gridLayout->addWidget(textLabel, i, 1);

                QPixmap pixmap;
                pixmap.convertFromImage(imageList[i]);
                pixmap.setDevicePixelRatio(0.5);
                imageLabel->setPixmap(pixmap);

                textLabel->setSizePolicy(QSizePolicy::MinimumExpanding, textLabel->sizePolicy().verticalPolicy());
                textLabel->setText(tr("Frame %1").arg(i+1));
            }

            ui->stackedWidget->setCurrentIndex(4);
            break;
        }
        case DataProviderType::Font:
        {
            ui->glyphComboBox->clear();
            const QVector<QPair<QChar, QImage>> glyphs = static_cast<FontProvider *>(provider)->readGlyphs();
            for (const QPair<QChar, QImage> & glyph : glyphs) {
                ui->glyphComboBox->addItem(glyph.first, glyph.second);
            }
            ui->stackedWidget->setCurrentIndex(5);
            break;
        }
        default:
            ui->stackedWidget->setCurrentIndex(0);
            break;
    }
}

void MainWindow::on_saveScriptButton_clicked()
{
    QTreeWidgetItem * const currentItem = ui->treeWidget->currentItem();
    if (!currentItem)
        return;

    DataProvider * const provider = currentItem->data(0, Qt::UserRole).value<DataProvider *>();
    if (!provider || provider->type() != DataProviderType::ScriptFile)
        return;

    if (QMessageBox::question(nullptr, tr("Warning"), tr("This will OVERWRITE your game data. Do you wish to proceed?"), QMessageBox::StandardButtons(QMessageBox::Yes | QMessageBox::No), QMessageBox::No) != QMessageBox::Yes)
        return;

    ScriptFileProvider * const scriptProvider = static_cast<ScriptFileProvider *>(provider);
    scriptProvider->writeScript(ui->scriptBrowser->toPlainText());
}

void MainWindow::on_glyphComboBox_currentIndexChanged(int index)
{
    if (index == -1)
    {
        ui->glyphImage->setPixmap(QPixmap());
        return;
    }

    QImage image = ui->glyphComboBox->itemData(index).value<QImage>();

    QPixmap pixmap;
    pixmap.convertFromImage(image);
    pixmap.setDevicePixelRatio(0.1);
    ui->glyphImage->setPixmap(pixmap);
}

void MainWindow::on_findEdit_returnPressed()
{
    searchScriptBrowser();
}

void MainWindow::on_findEdit_textEdited(const QString & text)
{
    Q_UNUSED(text);
    searchScriptBrowser();
}

bool MainWindow::eventFilter(QObject * watched, QEvent * event)
{
    if (event->type() == QEvent::KeyPress && (watched == ui->findEdit || watched == ui->scriptBrowser)) {
        QKeyEvent * keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->matches(QKeySequence::Find)) {
            ui->findWidget->show();
            ui->findEdit->setFocus();
            return true;
        }

        if (keyEvent->key() == Qt::Key_Escape) {
            ui->findWidget->hide();
            ui->scriptBrowser->setFocus();
            return true;
        }

        if (keyEvent->matches(QKeySequence::FindNext)) {
            searchScriptBrowser();
            return true;
        }
    }
    return false;
}
