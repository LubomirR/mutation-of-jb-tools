#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextCursor>
#include <QPalette>

class QTreeWidgetItem;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private slots:
        void on_actionOpen_triggered();

        void on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

        void on_saveScriptButton_clicked();

        void on_glyphComboBox_currentIndexChanged(int index);

        void on_findEdit_returnPressed();

        void on_findEdit_textEdited(const QString & text);

    protected:
        virtual bool eventFilter(QObject * watched, QEvent * event) override;

    private:
        Ui::MainWindow *ui;

        void buildTree(const QString & dir);
        void searchScriptBrowser();
};

#endif // MAINWINDOW_H
